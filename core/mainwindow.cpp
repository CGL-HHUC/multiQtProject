#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QHBoxLayout>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{

    this->setWindowTitle(QObject::trUtf8("测试中文标题"));
    this->setFixedSize(800,600);        // 设置窗口初始化大小

    this->widget = new CustomWidget();

    this->setCentralWidget(this->widget);

}

MainWindow::~MainWindow()
{
    if(widget == NULL)
    {
        delete widget;
    }
}
