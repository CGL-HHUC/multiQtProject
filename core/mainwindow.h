#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "../ui_test/customwidget.h"
#include <QMainWindow>



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:

    CustomWidget * widget;  // 自定义界面组件

};

#endif // MAINWINDOW_H
