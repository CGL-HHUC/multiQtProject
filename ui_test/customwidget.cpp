#include "customwidget.h"
#include<QHBoxLayout>   // 水平盒状分布
/************************
 * 构造函数
 * *********************/
CustomWidget::CustomWidget(QWidget * parent):
    QWidget(parent)
{
    this->label1 = new Ui_test();
    this->label1->setContent(QObject::tr("这是测试多个模块组成工程是否可以如此进行"));

    this->label2 = new Ui_test();
    this->label2->setContent(QObject::tr("core 和 ui_test 模块"));
    this->label3 = new Ui_test();

    QHBoxLayout* layout = new QHBoxLayout;     // 初始化一个布局管理
    layout->addWidget(this->label1);
    layout->addWidget(this->label2);
    layout->addWidget(this->label3);

    this->setLayout(layout);    //　设置布局管理

}

/********************
 * 析构函数
 * ******************/
CustomWidget::~CustomWidget()
{
    if(this->label1 != NULL)
    {
        delete this->label1;
    }

    if(this->label2 != NULL)
    {
        delete this->label2;
    }

    if(this->label3 != NULL)
    {
        delete this->label3;
    }
}
