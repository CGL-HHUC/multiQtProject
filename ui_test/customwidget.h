#ifndef CUSTOMWIDGET_H
#define CUSTOMWIDGET_H

#include <QWidget>
#include "ui_test.h"

class UI_TESTSHARED_EXPORT CustomWidget:public QWidget
{
    Q_OBJECT
public:
    CustomWidget(QWidget * parent = 0);
    ~CustomWidget();

private:
    Ui_test *label1;
    Ui_test *label2;
    Ui_test *label3;
};

#endif // CUSTOMWIDGET_H
