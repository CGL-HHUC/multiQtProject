#ifndef UI_TEST_H
#define UI_TEST_H

#include "ui_test_global.h"

#include <QLabel>



class UI_TESTSHARED_EXPORT Ui_test: public QLabel
{
    Q_OBJECT
public:
    Ui_test(QWidget *parent = 0);
    void setContent(QString &str);
};

#endif // UI_TEST_H
