#ifndef UI_TEST_GLOBAL_H
#define UI_TEST_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(UI_TEST_LIBRARY)
#  define UI_TESTSHARED_EXPORT Q_DECL_EXPORT
#else
#  define UI_TESTSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // UI_TEST_GLOBAL_H
